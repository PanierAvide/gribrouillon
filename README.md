# Gribrouillon

![Logo](src/website/img/logo.400.png)

Gribrouillon ([gribrouillon.fr](https://gribrouillon.fr)) makes drawing over web maps easy. You can create sketchy maps in seconds, on mobile or desktop. [Give it a try !](https://gribrouillon.fr/)

## Contributing

Interested in helping us ? No worries, you don't need to be a developer to do so. Here are some ways to help improving Gribrouillon :

* There is a bug or some feature missing ? You can either send us [an email](mailto:panieravide@riseup.net) or [create a new issue](https://framagit.org/PanierAvide/gribrouillon/issues) on the repository.
* If you speak English and another language, you can [help translate the tool](https://www.transifex.com/openlevelup/gribrouillon/) on Transifex.
* And as always, you can help solve bugs or implement feature if you know code.


## Contributors

Gribrouillon exists and is everyday improving thanks to the __amazing people__ following :

* Adrien Pavie, main developer
* Florian Lainez, user experience expert
* Trendspotter and idetao, for their translations
* And all users, for using the tool and sharing their ideas !


## Deploy your own instance

If you want to have your own Gribrouillon server running (either for developing or hosting), you can follow these instructions.

### Requirements

To run properly, Gribrouillon needs following software to be available :

* PostgreSQL >= 10
* PostGIS >= 2.4
* NodeJS >= 9
* Only for updating translations : [Transifex CLI](https://docs.transifex.com/client/installing-the-client)

### Install

```bash
# Retrieve source code
git clone https://framagit.org/PanierAvide/gribrouillon.git
cd gribrouillon

# Install JS dependencies
npm install

# Setup database
psql -c "CREATE DATABASE gribrouillon ENCODING 'UTF8'"
psql -d gribrouillon -f src/init.sql
```

### Configure

Edit `config.json` file according to your PostgreSQL setup.

### Run

```bash
npm run start

# Or alternatively, if you want to change port
PORT=12345 npm run start
```

Environment variables which can be used for API :

* `PORT` : for changing listening port (defaults to 3000)
* `BG_DL` : for setting how to retrieve background imagery file (values being `0` to disable usage, `1` for downloading from Internet, and `2` for reading from `res/imagery.geojson` file)


## License

Copyright (c) Adrien Pavie 2020

Released under AGPL 3 license, see [LICENSE](LICENSE.txt) for complete license text.
