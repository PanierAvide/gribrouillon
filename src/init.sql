CREATE EXTENSION IF NOT EXISTS postgis;

-- Random ID
CREATE OR REPLACE FUNCTION random_id(tablename VARCHAR, length INTEGER) RETURNS TEXT AS $$
DECLARE
	chars TEXT[] := '{0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z}';
	result TEXT := '';
	exists BOOLEAN;
	i INTEGER := 0;
BEGIN
	IF length < 0 THEN
		raise exception 'Given length cannot be less than 0';
	END IF;

	FOR i IN 1..length LOOP
		result := result || chars[1+random()*(array_length(chars, 1)-1)];
	END LOOP;

	-- Check if generated ID doesn't exist in table
	EXECUTE 'SELECT COUNT(*) > 0 AS exists FROM '||tablename||' WHERE id = $1' INTO exists USING result;

	IF exists THEN
		RETURN random_id(tablename, length);
	ELSE
		RETURN result;
	END IF;
END;
$$ LANGUAGE plpgsql;

-- List of existing sketches
CREATE TABLE IF NOT EXISTS sketch(
	id VARCHAR PRIMARY KEY DEFAULT random_id('sketch', 10),
	created TIMESTAMP NOT NULL DEFAULT current_timestamp,
	options JSON
);

-- List of drawn shapes in sketches
CREATE TABLE IF NOT EXISTS shape(
	id BIGSERIAL PRIMARY KEY,
	sketch VARCHAR REFERENCES sketch(id),
	created TIMESTAMP NOT NULL DEFAULT current_timestamp,
	geom GEOMETRY(Geometry, 4326) NOT NULL,
	properties JSON
);
CREATE INDEX IF NOT EXISTS shape_sketch_idx ON shape(sketch);

-- Clean-up function
CREATE OR REPLACE FUNCTION db_cleanup() RETURNS TEXT AS $$
BEGIN
	RAISE NOTICE 'Deleting old sketches without shapes';
	DELETE FROM sketch
	WHERE current_timestamp - created > '1 day' AND id NOT IN (SELECT DISTINCT sketch FROM shape);
	RETURN 'OK';
END;
$$ LANGUAGE plpgsql;
