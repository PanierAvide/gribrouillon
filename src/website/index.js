import I18n from "./i18n";

// Constants
const GEOCODER_URL = "https://nominatim.openstreetmap.org";
const BING_API_KEY = "AhzbK3kqqkO2lKw7imz7ofxWa5K_qPIFaktbhJhL-SmI_zQK2K36RaG1ULrXlmHZ";
const COLORS_BASE = [ "#000000", "#f0240f", "#f6cc00", "#00b953", "#654efa", "#b900f8" ];
const COLORS = [
	["white", "#d2d7d3", "#bfbfbf", "#67809f", "rgb(111, 111, 111)", "black"],
	["#ffffcc", "#f2d984", "#f0ff00", "#f5e51b", "#f4d03f", "#f7ca18"],
	["#fde3a7", "#f9bf3b", "#f39c12", "#f2784b", "#f9690e", "#d35400"],
	["#e08283", "#d24d57", "#f03434", "red", "#cf000f", "#96281b"],
	["#d5b8ff", "#be90d4", "#9b59b6", "#bf55ec", "#963694", "#663399"],
	["#c5eff7", "#81cfe0", "#22a7f0", "blue", "#2c3e50", "#1f3a93"],
	["#a2ded0", "#c8f7c5", "#26c281", "#00e640", "#049372", "green"]
];

// Script-wide variables
let map, sid, shapesLayer, tmpShapeCoordinates,
	tmpShapeLayer, timerGeocoder, noStartLocation,
	backgroundLayers, tilesLayer, backgroundLayersPosition,
	useBackgroundFromURL, mapLocCtrl;
const drawOpts = {
	color: "#f0240f",
	penSize: 10,
	opacity: 1
};
const shapesLayersById = {};
let shapeLayerTmpId = 0;
let selectedBackgroundLayer = "MAPNIK";
const shapesLayersWithoutId = {};
const socket = io();
let domBtnColor;
const domLoader = document.getElementById("loader");
const domBrushSizes = document.getElementById("brush-sizes");
const domColorPicker = document.getElementById("color-picker");
const domColorPickerLine = document.getElementById("color-picker-line");
const domBtnUndo = document.getElementById("btn-undo");
const domBtnShare = document.getElementById("btn-partageur");
const domBtnShareUrl = document.getElementById("btn-partageur-url");
const domSearchAddress = document.getElementById("searchbar");
const domBackgroundPicker = document.getElementById("background-picker");
const domBackgroundPickerMore = document.getElementById("more-bg");

// I18n
let locale = null;
if(navigator.languages) {
	for(let lng of navigator.languages) {
		if(I18n.supportedLocales.includes(lng)) {
			locale = lng;
			break;
		}
	}
}


/***********************************************************************************
 * Polyfills
 */

if(!Object.fromEntries) {
	Object.fromEntries = entries => {
		const obj = {};
		if(entries && entries.length > 0) {
			entries.forEach(e => {
				if(e && e.length > 0) {
					obj[e[0]] = e.length > 1 ? e[1] : undefined;
				}
			});
		}
		return obj;
	};
}



/***********************************************************************************
 * Leaflet rendering customization
 */

L.CircleMarkerZ = L.CircleMarker.extend({
	_update: function() {
		if(this._map) {
			// Compute actual radius
			if(this.options.z) {
				const z = this._map.getZoom();
				let newRadius = this.options.radius;
				if(z > this.options.z) {
					newRadius = newRadius * Math.pow(2, z - this.options.z);
				}
				else if(z < this.options.z) {
					newRadius = newRadius / Math.pow(2, this.options.z - z);
				}
				this._radius = Math.max(1, Math.round(newRadius));
			}

			this._updatePath();
		}
	}
});

L.Polyline.include({
	_update: function() {
		if (!this._map) { return; }

		// Compute actual weight
		if(this.options.z) {
			if(!this.options.originalWeight) { this.options.originalWeight = this.options.weight; }
			const z = this._map.getZoom();
			let newWeight = this.options.originalWeight;
			if(z > this.options.z) {
				newWeight = newWeight * Math.pow(2, z - this.options.z);
			}
			else if(z < this.options.z) {
				newWeight = newWeight / Math.pow(2, this.options.z - z);
			}
			this.options.weight = Math.max(1, Math.round(newWeight));
		}

		this._clipPoints();
		this._simplifyPoints();
		this._updatePath();
	}
});


/***********************************************************************************
 * API communication
 */

/**
 * Find available background layers
 */
function findBackgroundLayers() {
	const doJob = () => {
		try {
			backgroundLayersPosition = map.getCenter();
			socket.emit("background_find", [ backgroundLayersPosition.lng, backgroundLayersPosition.lat ]);
		}
		catch(e) {
			console.warn(e);
			setTimeout(doJob, 500);
		}
	}
	doJob();
}

/**
 * Loads an existing sketch
 * @param {string} id The sketch ID
 */
function loadSketch(id) {
	showLoader(true);
	changeMode("nav");

	socket.on("sketch_loaded", geojson => {
		sid = id;
		if(noStartLocation && geojson.bbox) {
			map.fitBounds([ [ geojson.bbox[1], geojson.bbox[0] ], [ geojson.bbox[3], geojson.bbox[2] ] ]);
		}
		loadBackgroundFromURL();
		updateRawDataLink();
		showLoader(false);
		showShapes(geojson);
		listenAPIChanges();
	});

	socket.on("sketch_load_failed", () => {
		showLoader(false);
		alert(I18n.t("Can't find given sketch. Check URL or retry later."));
	});

	socket.emit("sketch_load", id);
}

/**
 * Register a new sketch against API
 */
function registerSketch() {
	showLoader(true);
	changeMode("nav", { autoLocate: true });
	loadBackgroundFromURL();

	socket.on("sketch_created", id => {
		sid = id;
		showLoader(false);
		listenAPIChanges();
		updateRawDataLink();
		setUrlParameter("sid", sid);
	});

	socket.on("sketch_creation_failed", () => {
		showLoader(false);
		alert(I18n.t("Can't create a new sketch. Please retry later."));
	});

	socket.emit("sketch_create", {});
}

/**
 * Create a new shape
 */
function createShape(layer) {
	const feature = layer.toGeoJSON();
	feature.properties = Object.assign({}, layer.options);
	delete feature.properties.interactive;
	const tmpId = shapeLayerTmpId++;
	shapesLayersWithoutId[tmpId] = layer;
	socket.emit("shape_create", { sid: sid, feature: feature, tmpId: tmpId });
}

/**
 * Delete a shape
 */
function deleteShape(fid) {
	socket.emit("shape_delete", { fid: fid, sid: sid });
}

/**
 * Listen to upstream changes
 */
function listenAPIChanges() {
	// External shape creation
	socket.on("shape_created", showShapes);

	// Callback after own shape creation
	socket.on("shape_creation_failed", tmpId => {
		delete shapesLayersWithoutId[tmpId];
		alert(I18n.t("There was an error when saving your edits. Please reload the page."));
	});
	socket.on("shape_creation_done", opts => {
		shapesLayersById[opts.fid] = shapesLayersWithoutId[opts.tmpId];
		delete shapesLayersWithoutId[opts.tmpId];
		domBtnUndo.removeAttribute("disabled");
	});

	// Callback after own shape deletion
	socket.on("shape_deleted", fid => {
		shapesLayer.eachLayer(layer => {
			// Single feature
			if(layer.feature && layer.feature.id == fid) {
				shapesLayer.removeLayer(layer);
			}
			// GeoJSON collection
			else if(layer.eachLayer) {
				layer.eachLayer(sublayer => {
					if(sublayer.feature && sublayer.feature.id == fid) {
						layer.removeLayer(sublayer);
					}
				});
			}
		});
	});
	socket.on("shape_deletion_failed", fid => {
		alert(I18n.t("There was a deletion error. Please reload the page."));
	});
	socket.on("shape_deletion_done", fid => {
// 		console.log("Deleted shape", fid);
	});

	// Callback after background search
	socket.on("background_found", layers => {
		// Put best background first
		let newBgLayers = [];
		newBgLayers.push(layers.find(layer => layer.category === "photo") || { empty: true });
		newBgLayers.push(layers.find(layer => layer.id === "MAPNIK") || { empty: true });
		newBgLayers.push(layers.find(layer => layer.id === "osm-mapnik-black_and_white") || { empty: true });
		newBgLayers = newBgLayers.filter(layer => !layer.empty);

		// Then add other sorted
		layers.filter(layer => !newBgLayers.includes(layer))
		.forEach(layer => newBgLayers.push(layer));

		backgroundLayers = newBgLayers;

		if(useBackgroundFromURL) {
			const tile = backgroundLayers.find(bl => bl.id === useBackgroundFromURL);
			if(tile) {
				useBackgroundFromURL = null;
				showTiles(tile);
			}
			else {
				console.log("Background", useBackgroundFromURL, "not available in this area");
				useBackgroundFromURL = null;
			}
		}
		else {
			updateBackgroundsList();
		}
	});
}

/**
 * Ajax HTTP Get request
 */
function request(url) {
	return new Promise((resolve, reject) => {
		var xhr = new XMLHttpRequest();
		xhr.addEventListener("load", () => {
			resolve(xhr.responseText);
		});
		xhr.addEventListener("error", reject);
		xhr.addEventListener("abort", reject);
		xhr.open("GET", url);
		xhr.send();
	});
}

/**
 * Geocoding address
 */
function searchAddress(e, now) {
	if(timerGeocoder) {
		clearTimeout(timerGeocoder);
	}

	if(!now) {
		timerGeocoder = setTimeout(() => searchAddress(e, true), 1000);
	}
	else {
		showLoader(true);
		const url = GEOCODER_URL + `/search?q=${encodeURIComponent(e.target.value)}&format=json&limit=1`
		request(url)
		.then(txt => JSON.parse(txt))
		.then(data => {
			showLoader(false);

			if(data.length === 1 && data[0].boundingbox && data[0].boundingbox.length === 4) {
				const [ south, north, west, east ] = data[0].boundingbox.map(v => parseFloat(v));
				map.fitBounds([[ south, west ], [ north, east ]]);
			}
			else {
				alert(I18n.t("No result for your search, please check given address."));
			}
		})
		.catch(e => {
			showLoader(false);
			console.error(e);
			alert(I18n.t("Search service is unavailable. Please retry later."));
		});
	}
}



/***********************************************************************************
 * Drawing interactions
 */

/**
 * Clean-up map from temporary shapes
 */
function removeTmpShape() {
	tmpShapeCoordinates = null;
	if(tmpShapeLayer) {
		map.removeLayer(tmpShapeLayer);
		tmpShapeLayer = null;
	}
}

/**
 * Actions to perform once user ends drawing a shape
 */
function finishShapeDrawing(e) {
	if(tmpShapeCoordinates) {
		if(e && e.latlng) {
			tmpShapeCoordinates.push(e.latlng);
		}

		// Clean drawn line
		const cleanNodes = [];
		tmpShapeCoordinates.forEach((latlng, i) => {
			if(i === 0) {
				cleanNodes.push(latlng);
			}
			else if(
				cleanNodes.length > 0
				&& !latlng.equals(cleanNodes[cleanNodes.length-1])
				&& map.latLngToLayerPoint(latlng).distanceTo(map.latLngToLayerPoint(cleanNodes[cleanNodes.length-1])) >= 1
			) {
				cleanNodes.push(latlng);
			}
		});

		// Add clean geometry to map
		let layer;
		let layerOpts;
		if(cleanNodes.length === 1) {
			layerOpts = { stroke: false, fillColor: drawOpts.color, radius: drawOpts.penSize / 2, fillOpacity: drawOpts.opacity, z: map.getZoom(), interactive: false };
			layer = new L.CircleMarkerZ(cleanNodes[0], layerOpts);
		}
		else {
			layerOpts = { color: drawOpts.color, weight: drawOpts.penSize, opacity: drawOpts.opacity, z: map.getZoom(), interactive: false };
			layer = L.polyline(cleanNodes, layerOpts);
		}
		shapesLayer.addLayer(layer);

		// Send to API
		createShape(layer);
		removeTmpShape();
	}
}

/**
 * Event handler for when mouse is down
 */
function startTmpShape(e) {
	tmpShapeCoordinates = [ e.latlng ];
}

/**
 * Event handler for mouse move when drawing
 */
function continueTmpShape(e) {
	if(tmpShapeCoordinates) {
		tmpShapeCoordinates.push(e.latlng);
		if(tmpShapeCoordinates.length >= 2) {
			if(tmpShapeLayer) {
				tmpShapeLayer.setLatLngs(tmpShapeCoordinates);
			}
			else {
				tmpShapeLayer = L.polyline(tmpShapeCoordinates, { color: drawOpts.color, weight: drawOpts.penSize, opacity: drawOpts.opacity }).addTo(map);
			}
		}
	}
}

/**
 * Makes map drawable
 */
function enableDrawing() {
	map.on("mousedown", startTmpShape);
	map.on("mousemove", continueTmpShape);
	map.on("zoomanim", removeTmpShape);
	map.on("mouseup", finishShapeDrawing);
	document.getElementById("map").addEventListener("touchmove", e => {
		if(e.targetTouches.length > 1) {
			removeTmpShape(e);
		}
	});
	$("#map").mouseleave(finishShapeDrawing);
}

/**
 * Makes map not drawable anymore
 */
function disableDrawing() {
	finishShapeDrawing();
	map.off("mousedown", startTmpShape);
	map.off("mousemove", continueTmpShape);
	map.off("zoomanim", removeTmpShape);
	map.off("mouseup", finishShapeDrawing);
	$("#map").off("mouseleave", finishShapeDrawing);
}



/***********************************************************************************
 * Page management
 */

/**
 * Show of hide full page loader
 * @param {boolean} show True to show, false to hide
 */
function showLoader(show) {
	domLoader.classList.remove(show ? "d-none" : "d-flex");
	domLoader.classList.add(show ? "d-flex" : "d-none");
}

/**
 * Change the current mode of the map
 * @param {string} mode The mode (draw, nav)
 * @param {Object} [opts] Options
 */
function changeMode(mode, opts) {
	opts = opts || {};
	document.body.classList.remove("mode-draw", "mode-nav");
	document.body.classList.add("mode-"+mode);
	map.invalidateSize();

	switch(mode) {
		case "nav":
			disableDrawing();
			[ map.dragging, map.doubleClickZoom, map.scrollWheelZoom, map.touchZoom ].forEach(h => h.enable());
			map.zoomControl.addTo(map);
			map.addControl(mapLocCtrl);
			if(opts.autoLocate) {
				try {
					mapLocCtrl.start();
				}
				catch(e) {
					console.error(e);
				}
			}
			break;
		case "draw":
			[ map.dragging, map.doubleClickZoom, map.scrollWheelZoom, map.touchZoom ].forEach(h => h.disable());
			map.zoomControl.remove();
			map.removeControl(mapLocCtrl);
			domBtnColor.style.color = drawOpts.color;
			enableDrawing();
			break;
	}
}

/**
 * Display one or several shapes on map
 * @param {Object} geojson The feature or feature collection to display
 */
function showShapes(geojson) {
	const layer = L.geoJSON(geojson, {
		style: feature => Object.assign({ interactive: false }, feature.properties),
		pointToLayer: (feature, latlng) => new L.CircleMarkerZ(latlng, Object.assign({ interactive: false }, feature.properties))
	});
	shapesLayer.addLayer(layer);
}

/**
 * Remove the last drawn shape
 */
function undoShapeDrawn() {
	const fids = Object.keys(shapesLayersById).map(k => parseInt(k)).sort();

	if(fids.length <= 1) {
		domBtnUndo.setAttribute("disabled", "true");
	}

	if(fids.length > 0) {
		const fid = fids.pop();
		shapesLayer.removeLayer(shapesLayersById[fid]);
		delete shapesLayersById[fid];
		socket.emit("shape_delete", { fid: fid, sid: sid });
	}
}

/**
 * Change pen size
 */
function setPenSize(size) {
	drawOpts.penSize = parseInt(size);

	for(let i=0; i < domBrushSizes.childNodes.length; i++) {
		const domBtn = domBrushSizes.childNodes[i];
		if(domBtn.nodeName.toLowerCase() === "button") {
			const size = parseInt(domBtn.getAttribute("data-size"));
			if(size === drawOpts.penSize) { domBtn.classList.add("active"); }
			else { domBtn.classList.remove("active"); }
		}
	}
}

/**
 * Change drawing color
 */
function setColor(color) {
	drawOpts.color = color;

	let found = false;
	for(let i=0; i < domColorPickerLine.childNodes.length; i++) {
		const child = domColorPickerLine.childNodes[i];
		if(i === domColorPickerLine.childNodes.length - 1 && !found) {
			child.classList.add("btn-color-selected");
		}
		else if(child.getAttribute("data-color") === color) {
			found = true;
			child.classList.add("btn-color-selected");
		}
		else {
			child.classList.remove("btn-color-selected");
		}
	}
}

/**
 * Display modal for sharing map link
 */
function showShareModal() {
	// Show native share modal
	if(navigator && navigator.share) {
		navigator.share({
			url: location.href,
			title: I18n.t("Sketch map made with Gribrouillon")
		});
	}
	// Show custom modal
	else {
		// QR Code
		document.getElementById("share-qrcode").innerHTML = new QRCode({
			content: location.href,
			width: 200,
			height: 200,
			padding: 2,
			join: true,
			pretty: false
		}).svg();

		// Other links
		document.getElementById("share-url").value = location.href;
		document.getElementById("share-courriel").href = `mailto:?subject=${encodeURIComponent(I18n.t("Sketch map made with Gribrouillon"))}&body=${encodeURIComponent("See "+location.href)}`;

		// Open modal
		$("#sharingModal").modal("show");
	}
}

/**
 * Updates the link to raw map data
 */
function updateRawDataLink() {
	document.getElementById("btn-rawdata").href = "/raw/?sid="+sid;
}

/**
 * Replace background tiles by given ones
 * @param {Object} tile The imagery info
 */
function showTiles(tile) {
	if(selectedBackgroundLayer === tile.id) { return null; }

	selectedBackgroundLayer = tile.id;
	setUrlParameter("bg", tile.id);
	map.removeLayer(tilesLayer);

	switch(tile.type) {
		case "tms":
			tilesLayer = L.tileLayer(tile.url, tile);
			break;
		case "wms":
			tilesLayer = L.tileLayer.wms(tile.url, tile);
			break;
		case "wmts":
			tilesLayer = new L.TileLayer.WMTS(tile.url, tile);
			break;
		case "bing":
			tilesLayer = L.tileLayer.bing(BING_API_KEY);
			break;
	}

	tilesLayer.addTo(map);
}

function long2tile(lon,zoom) {
	return (Math.floor((lon+180)/360*Math.pow(2,zoom)));
}

function lat2tile(lat,zoom) {
	return (Math.floor((1-Math.log(Math.tan(lat*Math.PI/180) + 1/Math.cos(lat*Math.PI/180))/Math.PI)/2 *Math.pow(2,zoom)));
}

function tileToUrl(tile) {
	const center = map.getCenter();
	const zoom = Math.min(16, map.getZoom()+1);
	const y = lat2tile(center.lat, zoom);

	return tile.url
		.replace(/{x}/g, long2tile(center.lng, zoom))
		.replace(/{y}/g, y)
		.replace(/{-y}/g, y)
		.replace(/{z}/g, zoom)
		.replace(/{proj}/g, 'EPSG:3857')
		.replace(/{width}/g, 256)
		.replace(/{height}/g, 256)
		.replace(/{bbox}/g, map.getBounds().toBBoxString())
		.replace(/{s}/g, tile.subdomains && tile.subdomains.length > 0 ? tile.subdomains[0] : "a");
}

/**
 * Update list of background maps
 */
function updateBackgroundsList() {
	if(!backgroundLayers || !backgroundLayersPosition || backgroundLayersPosition.distanceTo(map.getCenter()) > 15000) {
		domBackgroundPicker.innerHTML = `<div class="spinner-border text-primary" role="status"><span class="sr-only">${I18n.t("Loading")}...</span></div>`;
		findBackgroundLayers();
	}
	else {
		domBackgroundPicker.innerHTML = '';

		// Create entry for each tile
		backgroundLayers.forEach((layer, i) => {
			const domBgl = document.createElement("div");
			domBgl.classList.add("card", "card-bg");
			if(i >= 3) { domBgl.classList.add("d-none"); }
			domBgl.setAttribute("data-dismiss", "modal");
			if(layer.id === selectedBackgroundLayer) {
				domBgl.classList.add("card-selected");
			}

			// Image
			const domBglImg = document.createElement("img");
			switch(layer.type) {
				case "tms":
				case "wms":
				case "wmts":
					domBglImg.src = tileToUrl(layer);
					break;
				case "bing":
					domBglImg.src = "img/bing.svg";
					break;
				default:
					domBglImg.src = "img/badge.svg";
			}
			domBglImg.alt = "";
			domBglImg.title = layer.name;
			domBglImg.classList.add("card-img-top");
			domBgl.appendChild(domBglImg);

			const domBglDiv = document.createElement("div");
			domBglDiv.classList.add("card-body", "p-2");
			domBglDiv.innerHTML = layer.name.length > 30 ? layer.name.substring(0, 27)+"..." : layer.name;

			domBgl.appendChild(domBglDiv);
			domBackgroundPicker.appendChild(domBgl);

			// Click event
			domBgl.addEventListener("click", () => showTiles(layer));
		});

		if(backgroundLayers.length > 3) {
			domBackgroundPickerMore.classList.remove("d-none");
		}
	}
}

/**
 * Force loading background if bg param exists in URL
 */
function loadBackgroundFromURL() {
	const params = readUrlParameters();
	if(params.bg && params.bg !== "MAPNIK") {
		useBackgroundFromURL = params.bg;
		findBackgroundLayers();
	}
}

/**
 * Function to hide left menu
 */
function hideLeftMenu() {
	document.getElementById("left-menu").classList.add("d-none");
}

/**
 * Read all params from URL
 */
function readUrlParameters() {
	if(!location.search || location.search.trim() === "" || location.search.trim() === "?") { return {}; }
	else {
		return Object.fromEntries(
			location.search
			.replace(/^\?/, '')
			.split("&")
			.map(kv => kv.split("=").map(decodeURIComponent))
		);
	}
}

/**
 * Change a parameter in URL
 * @param {string} k The key
 * @param {string} v The value
 */
function setUrlParameter(k, v) {
	// Read current parameters
	const params = readUrlParameters();
	params[k] = v;
	const newSearch = Object.entries(params).map(e => e.map(encodeURIComponent).join("=")).join("&");
	const newUrl = [
		location.protocol, '//', location.host,
		location.pathname,
		"?"+newSearch,
		location.hash.length > 0 ? location.hash : ""
	].join('');
	window.history.replaceState({}, "", newUrl);
}

/**
 * Initializes the editor, with default settings
 */
function init() {
	// Left menu
	document.getElementById("btn-menu").addEventListener("click", () => {
		document.getElementById("left-menu").classList.remove("d-none");
	});
	document.getElementById("left-menu-mask").addEventListener("click", hideLeftMenu);
	document.getElementById("btn-background").addEventListener("click", hideLeftMenu);

	// Buttons
	document.getElementById("btn-valid").addEventListener("click", () => changeMode("nav"));
	document.getElementById("btn-edit").addEventListener("click", () => changeMode("draw"));
	domBtnShare.addEventListener("click", showShareModal);
	domBtnUndo.addEventListener("click", undoShapeDrawn);
	domBackgroundPickerMore.addEventListener("click", e => {
		document.querySelectorAll(".card-bg.d-none").forEach(d => d.classList.remove("d-none"));
		domBackgroundPickerMore.classList.add("d-none");
	});
	$("#backgroundModal").on("show.bs.modal", updateBackgroundsList);
	domBtnShareUrl.addEventListener("click", e => {
		// Copy text to clipboard
		const txtField = document.getElementById("share-url");
		txtField.select();
		txtField.setSelectionRange(0, 99999);
		document.execCommand("copy");

		// Unselect text field
		txtField.blur();
		if(window.getSelection) {
			window.getSelection().removeAllRanges();
		}
		else if (document.selection) {
			document.selection.empty();
		}

		// Animation for button
		domBtnShareUrl.classList.add("btn-success");
		domBtnShareUrl.classList.remove("btn-secondary");
		setTimeout(() => {
			domBtnShareUrl.classList.remove("btn-success");
			domBtnShareUrl.classList.add("btn-secondary");
		}, 1000);
	});

	// Brush sizes
	let setActive = true;
	for(let i=0; i < domBrushSizes.childNodes.length; i++) {
		const domBtn = domBrushSizes.childNodes[i];
		if(domBtn.nodeName.toLowerCase() === "button") {
			const size = parseInt(domBtn.getAttribute("data-size"));
			if(setActive) {
				domBtn.classList.add("active");
				setActive = false;
				drawOpts.penSize = size;
			}
			domBtn.addEventListener("click", () => setPenSize(size));
		}
	}

	// Color picker
	COLORS_BASE.forEach(color => {
		const domBtn = document.createElement("button");
		domBtn.classList.add("btn-color");
		domBtn.style.backgroundColor = color;
		domBtn.title = I18n.t("Change color");
		domBtn.setAttribute("data-color", color);
		domBtn.addEventListener("click", () => setColor(color));
		domColorPickerLine.appendChild(domBtn);
	});

	domBtnColor = document.createElement("button");
	domBtnColor.id = "btn-color";
	domBtnColor.classList.add("btn-color");
	domBtnColor.setAttribute("data-toggle", "modal");
	domBtnColor.setAttribute("data-target", "#colorModal");
	domColorPickerLine.appendChild(domBtnColor);

	COLORS.forEach(range => {
		const domRange = document.createElement("div");
		domRange.classList.add("d-flex", "justify-content-between", "align-items-center", "mt-2", "mb-2");
		range.forEach(color => {
			const domColor = document.createElement("button");
			domColor.classList.add("color-picker-color");
			domColor.style.backgroundColor = color;
			domColor.setAttribute("data-dismiss", "modal");
			domColor.addEventListener("click", () => setColor(color));
			domRange.appendChild(domColor);
		});
		domColorPicker.appendChild(domRange);
	});
	setColor(drawOpts.color);

	// Map
	map = L.map("map", {
		preferCanvas: true,
		boxZoom: false
	});

	map.attributionControl.setPrefix("Gribrouillon");
	map.addHash();
	noStartLocation = location.hash === "#1/0/0";
	if(location.hash.length === 0 || !/^#\d+\/-?\d+(\.\d+)?\/-?\d+(\.\d+)?$/.test(location.hash)) {
		map.setView([0,0], 1);
		noStartLocation = true;
	}

	mapLocCtrl = L.control.locate({
		setView: "untilPanOrZoom",
		onLocationError: e => { console.log("Location error", e.message); },
		locateOptions: { maxZoom: 19, enableHighAccuracy: true },
		strings: {
			title: I18n.t("Show my location on map"),
			metersUnit: I18n.t("meters"),
			feetUnit: I18n.t("feet"),
			popup: I18n.t("You are within {distance} {unit} around this point"),
			outsideMapBoundsMsg: I18n.t("You are located outside map bounds")
		},
		flyTo: true
	});

	tilesLayer = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
		maxZoom: 19,
		attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
	})
	.addTo(map);

	shapesLayer = L.layerGroup().addTo(map);

	// Geocoder
	domSearchAddress.value = "";
	domSearchAddress.addEventListener("input", searchAddress);

	// Translate various labels in HTML page
	document.querySelector("[data-i18n='loading']").innerHTML = I18n.t("Loading");
	document.querySelector("[data-i18n='changebg']").innerHTML = I18n.t("Change background");
	document.querySelector("[data-i18n='about']").innerHTML = I18n.t("About");
	document.querySelector("[data-i18n='rawdata']").innerHTML = I18n.t("Download raw map data");
	document.querySelector("[data-i18n='sourcecode']").innerHTML = I18n.t("Source code");
	document.querySelector("[data-i18n-title='menu']").title = I18n.t("Menu");
	document.querySelector("[data-i18n-title='search']").title = I18n.t("Search");
	document.querySelector("[data-i18n-title='share']").title = I18n.t("Share your sketch");
	document.querySelector("[data-i18n-title='vsmall']").title = I18n.t("Very small");
	document.querySelector("[data-i18n-title='small']").title = I18n.t("Small");
	document.querySelector("[data-i18n-title='medium']").title = I18n.t("Medium");
	document.querySelector("[data-i18n-title='large']").title = I18n.t("Large");
	document.querySelector("[data-i18n-title='undo']").title = I18n.t("Undo");
	document.querySelector("[data-i18n-title='more']").title = I18n.t("More");
	document.querySelector("[data-i18n-title='startdrawing']").title = I18n.t("Start drawing");
	document.querySelector("[data-i18n-title='stopdrawing']").title = I18n.t("Stop drawing");
	document.querySelector("[data-i18n-title='copy']").title = I18n.t("Copy");
	document.getElementById("colorModalLabel").innerHTML = I18n.t("Choose a color");
	document.querySelector("[data-i18n='sharemap']").innerHTML = I18n.t("Share your map");
	document.querySelector("[data-i18n='byemail']").innerHTML = I18n.t("By email");
	document.querySelector("[data-i18n='chmapbg']").innerHTML = I18n.t("Choose map background");
	document.querySelector("[data-i18n-pl='searchstreet']").placeholder = I18n.t("Search your street, city, country...");

	// Read sketch ID from URL
	const sidTxt = readUrlParameters().sid;
	if(sidTxt && sidTxt.trim().length > 0) {
		loadSketch(sidTxt.trim());
	}
	else {
		registerSketch();
	}
}

// Start only after setting correct language
I18n.changeLocale(locale || navigator.userLanguage || navigator.language)
.then(init);
