/**
 * PostgreSQL database connector
 */

const { Pool } = require('pg');
const CONFIG = require('../config.json');

const pool = new Pool({
	user: CONFIG.PG_USER,
	host: CONFIG.PG_HOST,
	database: CONFIG.PG_DB,
	port: CONFIG.PG_PORT
});

/**
 * Create a new sketch
 * @param {Object} options Various options
 * @return {Promise} A promise solving on sketch ID
 */
exports.createSketch = options => {
	return pool.query("INSERT INTO sketch(options) VALUES($1) RETURNING id", [ options ])
	.then(result => result.rows[0].id);
};

/**
 * Loads data from existing sketch
 * @param {string} sid The sketch ID
 * @return {Promise} Resolves on GeoJSON containing data from sketch
 */
exports.loadSketch = (sid) => {
	return pool.query("SELECT options FROM sketch WHERE id = $1", [ sid ])
	.then(result => {
		if(result.rows.length === 1) {
			const geojson = { type: "FeatureCollection", properties: result.rows[0].options };
			return pool.query(`
SELECT
	COALESCE(array_agg(json_build_object(
		'type', 'Feature',
		'geometry', ST_AsGeoJSON(geom)::json,
		'properties', properties,
		'id', id
	)), ARRAY[]::json[]) AS features,
	ST_AsGeoJSON(ST_Envelope(ST_Collect(geom)))::json AS envelope
FROM (
	SELECT *
	FROM shape
	WHERE sketch = $1
	ORDER BY created
) a
`, [ sid ])
			.then(result2 => {
				geojson.features = result2.rows[0].features;
				let bbox;
				try {
					bbox = result2.rows[0].envelope && result2.rows[0].envelope.coordinates[0];
					geojson.bbox = bbox && [ bbox[0][0], bbox[0][1], bbox[2][0], bbox[2][1] ];
				}
				catch(e) {
					console.error("Invalid bbox", bbox);
				}
				return geojson;
			});
		}
		else {
			return Promise.reject(new Error("Can't find given sketch"));
		}
	});
};

/**
 * List all interesting sketches from most recent to oldest one
 * @return {Promise} A promise solving on sketches metadata
 */
exports.listSketches = () => {
	return pool.query("SELECT s.id, s.created FROM sketch s JOIN shape sh ON s.id = sh.sketch GROUP BY s.id HAVING COUNT(sh.id) >= 2 ORDER BY s.created DESC")
	.then(result => result.rows);
};

/**
 * Create a new shape
 * @param {string} sid The sketch ID
 * @param {Object} feature The shape GeoJSON feature
 * @return {Promise} A promise solving on shape ID
 */
exports.createShape = (sid, feature) => {
	return pool.query(
		"INSERT INTO shape(sketch, geom, properties) VALUES($1, ST_GeomFromGeoJSON($2), $3) RETURNING id",
		[
			sid,
			feature.geometry,
			feature.properties
		]
	)
	.then(result => result.rows[0].id);
};

/**
 * Deletes existing shape
 * @param {string} sid The sketch ID
 * @param {int} fid The shape ID
 * @return {Promise} Resolves on success
 */
exports.deleteShape = (sid, fid) => {
	return pool.query(
		"DELETE FROM shape WHERE id = $1 AND sketch = $2 RETURNING id",
		[ fid, sid ]
	)
	.then(result => {
		if(result.rows.length === 1) {
			return true;
		}
		else {
			return Promise.reject(new Error("Shape was not found"));
		}
	});
};
