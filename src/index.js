/**
 * API main code
 */

const CONFIG = require('../config.json');
const express = require('express');
const cors = require('cors');
const basicAuth = require('express-basic-auth');
const db = require("./db");
const request = require("request-promise-native");
const fs = require("fs");
const booleanContains = require("@turf/boolean-contains").default;

// Init API
const port = process.env.PORT || 3000;
const app = express();
const http = require("http").createServer(app);
const io = require("socket.io")(http);
app.use(cors());
app.options('*', cors());
app.use(require("compression")());


/*
 * Retrieve editor layer index
 */

let backgroundLayers = [
	{ id: "MAPNIK", name: "OpenStreetMap (standard)", type: "tms", category: "osmbasedmap", url: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", attribution: "© OpenStreetMap contributors, CC-BY-SA 2.0", geometry: null }
];
const exclusionList = [
	"EOXAT2018CLOUDLESS", "osmbe-nl", "osmbe-fr", "osmbe", "openpt_map", "osm-mapnik-no_labels",
	"osmfr", "osm-mapnik-german_style", "tf-landscape", "tf-cycle", "wikimedia-map", "HDM_HOT",
	"osmie_tie_not_counties", "GSGS4136"
];
const fixCategory = { "Bing": "photo", "fr.ign.bdortho": "photo" };

function parseSaveBackgroundLayers(geojson) {
	const toKeep = geojson.features.filter(layer => {
		const p = layer.properties;
		p.category = p.category || fixCategory[p.id] || "other";
		if(p.category === "other" && p.name.toLowerCase().includes("aerial")) { p.category = "photo"; }

		return p.id && p.url && p.name
			&& !exclusionList.includes(p.id)
			&& ["tms", "wms", "bing", "wmts"].includes(p.type)
			&& !["qa", "elevation", "other"].includes(p.category)
			&& (!p.min_zoom || p.min_zoom <= 5)
			&& (!p.max_zoom || p.max_zoom >= 14)
			&& !p.overlay;
	})
	.map(layer => {
		const p = layer.properties;

		let subdomains = undefined;
		if(p.url.includes("{switch")) {
			const search = /{switch:(.+?)}/g.exec(p.url);
			if(search.length >= 1) { subdomains = search[1].split(","); }
		}

		let attribution = p.attribution && p.attribution.text ? p.attribution.text : undefined;
		if(p.attribution && p.attribution.url) {
			attribution = `<a href="${p.attribution.url}" target="_blank">${attribution || "License"}</a>`;
		}

		let wmsParams = p.type === "wms" ? {} : null;

		return {
			id: p.id,
			name: p.name,
			type: p.type,
			category: p.category,
			url: p.url
				.replace(/{zoom}/g, '{z}')
				.replace(/{switch:.+?}/g, '{s}'),
			attribution: attribution,
			geometry: layer.geometry,
			best: p.best || false,
			minZoom: p.min_zoom || 0,
			maxZoom: p.max_zoom || 18,
			subdomains: subdomains,
			wmsParams: wmsParams,
			end_date: p.end_date || (new Date()).toISOString().split("Z")[0]
		};
	});

	if(toKeep.length > 0) {
		backgroundLayers = toKeep;
		console.log("Updated background layers");
	}
}

function retrieveBackgroundLayers() {
	// Read from local file
	if(process.env.BG_DL && parseInt(process.env.BG_DL) === 2) {
		fs.readFile("res/imagery.geojson", (err, data) => {
			if(err) { console.error(err); }
			else {
				parseSaveBackgroundLayers(JSON.parse(data));
			}
		});
	}
	// Download from Internet
	else {
		request("https://osmlab.github.io/editor-layer-index/imagery.geojson")
		.then(txt => JSON.parse(txt))
		.then(parseSaveBackgroundLayers)
		.catch(e => {
			console.error(e);
			console.error("Can't retrieve background layers");
		});
	}
}

if(!process.env.BG_DL || parseInt(process.env.BG_DL) === 1) {
	setTimeout(retrieveBackgroundLayers, 15000); // Delay first call
	setInterval(retrieveBackgroundLayers, 1000*60*60*24); // Once per day
}
else if(process.env.BG_DL && parseInt(process.env.BG_DL) === 2) {
	setTimeout(retrieveBackgroundLayers, 2500); // Delay first call
	console.log("Read background imagery config from local file");
}
else {
	console.log("Background imagery download disabled");
}



/*
 * List of routes
 */

app.use('/', express.static(__dirname+'/../build'));

app.get('/status', (req, res) => {
	res.send({ "status": "ok" });
});

app.get('/raw/', (req, res) => {
	if(typeof req.query.sid !== "string") {
		return res.status(400).send("Sketch ID is missing or invalid");
	}

	db.loadSketch(req.query.sid)
	.then(geojson => {
		// Append uMap options for styling
		for(let i in geojson.features) {
			geojson.features[i].properties._umap_options = Object.assign({}, geojson.features[i].properties);
		}
		res.send(geojson);
	})
	.catch(e => {
		console.error(e);
		res.status(500).send("Error when connecting to database");
	});
});

app.get('/admin/sketches', basicAuth({ challenge: true, users: { [CONFIG.ADMIN_USER]: CONFIG.ADMIN_PASS } }), (req, res) => {
	fs.readFile("src/website/admin.html", "utf8", (err, data) => {
		if(err) {
			console.error(err);
			return res.status(500).send("Can't load admin page");
		}

		db.listSketches()
		.then(sketches => {
			res.send(data.replace("[/* SKETCHES_LIST */]", JSON.stringify(sketches)));
		})
		.catch(e => {
			console.error(e);
			res.status(500).send("Can't access sketch list");
		});
	});
});

// 404
app.use((req, res) => {
	res.status(404).send(req.originalUrl + ' not found')
});


/*
 * Sketch synchronisation through socket
 */

io.on("connection", socket => {
	// Find available backgrounds
	socket.on("background_find", coordinates => {
		// Get all layers available in this area
		const potential = {};
		backgroundLayers.filter(layer => (
			layer.geometry == null
			|| (
				layer.geometry.type === "MultiPolygon"
				&& layer.geometry.coordinates.find(lgc => booleanContains({ type: "Polygon", coordinates: lgc }, { type: "Point", coordinates: coordinates })))
			|| (
				layer.geometry.type !== "MultiPolygon"
				&& booleanContains(layer.geometry, { type: "Point", coordinates: coordinates }))
		))
		.forEach(layer => {
			if(!potential[layer.category]) { potential[layer.category] = [ layer ]; }
			else { potential[layer.category].push(layer); }
		});

		// Limit to 2 per category
		let toSend = [];
		Object.entries(potential)
		.forEach(e => {
			const [ cat, layers ] = e;
			layers.sort((a, b) => {
				if(a.best || b.best) {
					return a.best ? (b.best ? 0 : -1) : 1;
				}
				else {
					return (new Date(a.end_date)).getTime() - (new Date(b.end_date)).getTime();
				}
			});
			toSend = toSend.concat(["photo","historicphoto"].includes(cat) ? layers.slice(0, 2) : layers);
		});

		socket.emit("background_found", toSend);
	});

	// Create new sketch
	socket.on("sketch_create", options => {
		db.createSketch(options)
		.then(id => {
			socket.emit("sketch_created", id);
			socket.join("sketch_"+id);
		})
		.catch(e => {
			console.error(e);
			socket.emit("sketch_creation_failed");
		});
	});

	// Retrieve existing sketch
	socket.on("sketch_load", id => {
		db.loadSketch(id)
		.then(geojson => {
			socket.emit("sketch_loaded", geojson);
			socket.join("sketch_"+id);
		})
		.catch(e => {
			console.error(e);
			socket.emit("sketch_load_failed");
		});
	});

	// Add new shape to sketch
	socket.on("shape_create", opts => {
		db.createShape(opts.sid, opts.feature)
		.then(fid => {
			opts.feature.id = fid;
			if(!socket._createdShapes) { socket._createdShapes = []; }
			socket._createdShapes.push(fid);
			socket.to("sketch_"+opts.sid).emit("shape_created", opts.feature);
			socket.emit("shape_creation_done", { fid: fid, tmpId: opts.tmpId });
		})
		.catch(e => {
			console.error(e);
			socket.emit("shape_creation_failed", opts.tmpId);
		});
	});

	// Delete created shape
	socket.on("shape_delete", opts => {
		if(socket._createdShapes && socket._createdShapes.includes(opts.fid.toString())) {
			db.deleteShape(opts.sid, opts.fid)
			.then(() => {
				socket.to("sketch_"+opts.sid).emit("shape_deleted", opts.fid);
				socket.emit("shape_deletion_done", opts.fid);
			})
			.catch(e => {
				console.error(e);
				socket.emit("shape_deletion_failed", opts.fid);
			});
		}
		else {
			socket.emit("shape_deletion_failed", opts.fid);
		}
	});
});


/*
 * Start HTTP server
 */
http.listen(port, () => {
	console.log('API started on port: ' + port);
});
