# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## Next version (to be released)

### Added
- Very basic admin page to display recent sketches


## 0.2.2 - 2020-03-04

### Added
- Translation in Czech and Italian, making Gribrouillon available in 4 languages with English and French


## 0.2.1 - 2020-02-17

### Added
- In API, list of background imagery can be stored locally

### Changed
- List of available background imagery is limited to most pertinent ones, and shows only 3 by default (aerial imagery, OSM, OSM black & white)

### Fixed
- Geometry sorted by date
- Avoid partial drawing when double-tap on mobile


## 0.2.0 - 2020-02-14

### Added
- If no map position is provided when loading an existing sketch, it zooms automatically to existing shapes
- Map background can be changed using left menu
- Geolocation button added
- Translation system

### Changed
- Drawn shapes are now more or less wide according to zoom level
- Color selector has now presets + modal selector
- Brush size selector are now four buttons
- Share button uses native navigator sharing modal if available

### Removed
- Button for SMS link sharing (just doesn't work)
- Button for email sharing only shows on desktop


## 0.1.1 - 2020-02-11

### Added
- User can undo last drawn features


## 0.1.0 - 2020-02-11

### Added
- Initial release (UI, backend)
